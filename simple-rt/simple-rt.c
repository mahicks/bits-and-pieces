/*-----------------------------------------------------------------------------
 *
 *                    Simple Self-contained C Ray-Tracer
 *
 *                                M.A. Hicks
 *
 *---------------------------------------------------------------------------*/

#include <stdio.h>  /* for file i/o          */
#include <stdlib.h> /* for malloc            */
#include <math.h>   /* for sqrt              */
#include <stdint.h> /* for fixed-width types */
#include <float.h>  /* for DBL_MAX           */

/*--------------------------- Data Structures ------------------------------ */

#define MAX_OBJECTS 100
#define MAX_LIGHTS  2
typedef uint32_t img_t;

struct vect_struct {
  double x,y,z;
};
typedef struct vect_struct vect;

enum object_type {NONE,SPHERE};
typedef enum object_type object_type;

struct object_struct {
  object_type type;
  img_t       col;
  vect        origin, p1;
  float       diff;  /* diffusion coef (0.0..1.0) */
  float       shiny; /* shinyness coef (0.0..1.0) */
};
typedef struct object_struct object;

struct ray_struct {
  vect origin, dir;
};
typedef struct ray_struct ray;

struct plane_struct {
  vect tlc, xdir, ydir;
};
typedef struct plane_struct plane;

struct image_struct {
  unsigned char *data;
  unsigned int  width, height;
};
typedef struct image_struct image;

struct scene_struct {
  vect         camera;
  plane        view_plane;
  object       objects[MAX_OBJECTS];
  object       lights[MAX_LIGHTS];
  unsigned int num_objects, num_lights;
  double       ambient;
  unsigned     reflections;
};
typedef struct scene_struct scene;

/*----------------------------Main Methods---------------------------------- */


void alloc_image(image* targ_image);
void dealloc_image(image* targ_image);
double intersect_sphere(const ray *this_ray, const object *the_sphere,
                        vect *int_point);
const object* intersect(const ray *this_ray, const object *objects,
                        unsigned numobjs, vect *int_point, double* rdist);
vect point_normal(const vect *point, const object *intobj);
img_t shade_normal(const vect *cam, const vect *int_point,
                   const object *int_object, const object *the_light);
img_t shade(const ray *src_ray,const vect *int_point, const object *int_object,
            const object *lights, unsigned int num_lights, double ambient,
            unsigned reflections, const object *objects,
            unsigned int objects_num);
const object* trace_ray(img_t *shadev, const ray *new_ray,const object *lights,
                        unsigned int num_lights, double ambient,
                        unsigned reflections, const object *objects,
                        unsigned int num_objects);
int render_scene(scene *src_scene, image *targ_image, double image_scale);

/*----------------------------- Definitions ---------------------------------*/

static vect vect_add(const vect *v1, const vect *v2) {
  vect res;
  res.x = v1->x + v2->x;
  res.y = v1->y + v2->y;
  res.z = v1->z + v2->z;
  return res;
}

static vect vect_sub(const vect *v1, const vect *v2) {
  vect res;
  res.x = v1->x - v2->x;
  res.y = v1->y - v2->y;
  res.z = v1->z - v2->z;
  return res;
}

static vect vect_scale(const vect *v1, double scaler) {
  vect res;
  res.x = v1->x * scaler;
  res.y = v1->y * scaler;
  res.z = v1->z * scaler;
  return res;
}

static double vect_mag(const vect *v1) {
  vect tmp;
  tmp.x = v1->x * v1->x;
  tmp.y = v1->y * v1->y;
  tmp.z = v1->z * v1->z;
  return sqrt(tmp.x + tmp.y + tmp.z);
}

static vect vect_norm(const vect *v1) {
  vect res;
  double mag = vect_mag(v1);
  res.x = v1->x / mag;
  res.y = v1->y / mag;
  res.z = v1->z / mag;
  return res;
}

static double vect_dot(const vect *v1, const vect *v2) {
  return (v1->x * v2->x + v1->y * v2->y + v1->z * v2->z);
}

static vect vect_reflect(const vect *v1, const vect* norm) {
  vect res = {0};
  /* res = 2(v1 . norm)norm - v1  */
  double refdot = vect_dot(v1,norm) * 2.0;
  res = vect_scale(norm,refdot);
  res = vect_sub(v1,&res);
  return res;
}

static int ep_equal(double a, double b) {
  static const double ep_val = 0.0000001;
  int res = 0;  /* default case: not equal */
  if (a == b) { /* simple equality, stop early */
    res = 1;
  }
  else {        /* use epsilon diff to look for equality */
    if (a > b) {
      if ((a - ep_val) <= b) {
        res = 1;
      }
    }
    else {
      if ((b - ep_val) <= a) {
        res = 1;
      }
    }
  }
  return res;
}

static double clamp(double value, double clamp) {
  if (value > clamp)
    return clamp;
  else
    return value;
}

static img_t rgba(unsigned char r, unsigned char g, unsigned char b,
                  unsigned char a) {
  img_t res = (0xFF << 24);
  res = res |  r;
  res = res | (g << 8);
  res = res | (b << 16);
  res = res | (a << 24);
  return res;
}

static img_t rgb(unsigned char r, unsigned char g, unsigned char b) {
  return rgba(r,g,b,0xFF);
}

static img_t rgba_mix(img_t col1, img_t col2) {
  unsigned char r1 = (col1 & 0x000000FF),
                g1 = (col1 & 0x0000FF00) >> 8,
                b1 = (col1 & 0x00FF0000) >> 16,
                a1 = (col1 & 0xFF000000) >> 24,
                r2 = (col2 & 0x000000FF),
                g2 = (col2 & 0x0000FF00) >> 8,
                b2 = (col2 & 0x00FF0000) >> 16,
                a2 = (col2 & 0xFF000000) >> 24;
  unsigned char r3 = ((r1+r2)/2),
                g3 = ((g1+g2)/2),
                b3 = ((b1+b2)/2),
                a3 = ((a1+a2)/2);
  return rgba(r3,g3,b3,a3);
}

static img_t rgba_chan_scale(img_t col1, img_t chan_scale) {
  unsigned char r1 = (col1 & 0x000000FF),
                g1 = (col1 & 0x0000FF00) >> 8,
                b1 = (col1 & 0x00FF0000) >> 16,
                a1 = (col1 & 0xFF000000) >> 24,
                r2 = (chan_scale & 0x000000FF),
                g2 = (chan_scale & 0x0000FF00) >> 8,
                b2 = (chan_scale & 0x00FF0000) >> 16,
                a2 = (chan_scale & 0xFF000000) >> 24;
  unsigned char r3 = ( (double)r1 * ( (double)r2 / (double)255) ),
                g3 = ( (double)g1 * ( (double)g2 / (double)255) ),
                b3 = ( (double)b1 * ( (double)b2 / (double)255) ),
                a3 = ( (double)a1 * ( (double)a2 / (double)255) );
  return rgba(r3,g3,b3,a3);
}

static img_t rgba_add(img_t col1, img_t col2) {
  unsigned char r1 = (col1 & 0x000000FF),
                g1 = (col1 & 0x0000FF00) >> 8,
                b1 = (col1 & 0x00FF0000) >> 16,
                a1 = (col1 & 0xFF000000) >> 24,
                r2 = (col2 & 0x000000FF),
                g2 = (col2 & 0x0000FF00) >> 8,
                b2 = (col2 & 0x00FF0000) >> 16,
                a2 = (col2 & 0xFF000000) >> 24;
  unsigned char r3 = clamp(r1+r2,255),
                g3 = clamp(g1+g2,255),
                b3 = clamp(b1+b2,255),
                a3 = clamp(a1+a2,255);
  return rgba(r3,g3,b3,a3);
}

static img_t rgba_scale(img_t col, double scale) {
  unsigned char r = (unsigned char)(scale * (double)(col & 0x000000FF)),
                g = (unsigned char)(scale * (double)((col & 0x0000FF00)>>8)),
                b = (unsigned char)(scale * (double)((col & 0x00FF0000)>>16)),
                a = (unsigned char)(scale * (double)((col & 0xFF000000)>>24));
  return rgba(r,g,b,a);
}

static vect plane_coord(const plane *p1, double x, double y, double scale) {
  vect res, xcomp, ycomp;
  xcomp = vect_scale(&p1->xdir, x*scale);
  ycomp = vect_scale(&p1->ydir, y*scale);
  res = vect_add(&xcomp,&ycomp);
  res = vect_add(&res,&p1->tlc);
  return res;
}

void alloc_image(image* targ_image) {
  targ_image->data =
              calloc(targ_image->width  * targ_image->height, sizeof(img_t));
}

void dealloc_image(image* targ_image) {
  free(targ_image->data);
}

double intersect_sphere(const ray *this_ray, const object *the_sphere,
                               vect *int_point) {
  double       dist  = -1;
  vect         tmpv1 = {0};
  double       tmpd1 = 0.0, tmpd2 = 0.0, tmpd3 = 0.0;
  /* (l.(o-c))^2 - ||o-c||^2 + r^2 */
  /* tmpv1 = o - c */
  tmpv1 = vect_sub(&this_ray->origin,&the_sphere->origin);
  /* tmpd1 = l.(o-c) */
  tmpd1 = vect_dot(&this_ray->dir,&tmpv1);
  /* first sol. */
  tmpd3 = vect_mag(&tmpv1);
  tmpd2 = (tmpd1 * tmpd1) - (tmpd3 * tmpd3) +
          (the_sphere->p1.x * the_sphere->p1.x);
  /* evaluate intersections */
  if (tmpd2 >= 0) { /* 1 or 2 intersections - calc distance*/
    /* -(l.(o-c)) + sqrt(previous-equation) */
    tmpd3 = -(tmpd1) - sqrt(tmpd2);
    dist = tmpd3;
    /* provide the int point */
    *int_point = vect_scale(&this_ray->dir, dist);
    *int_point = vect_add(&this_ray->origin,int_point);
  }
  return dist;
}

const object* intersect(const ray *this_ray, const object *objects,
                        unsigned numobjs, vect *int_point, double *rdist) {
  const object *res  = 0;
  double        dist  = DBL_MAX;
  double        tmpdist;
  unsigned int  obj;
  for (obj = 0; obj < numobjs; obj++) {
    const object *the_obj = &objects[obj];
    tmpdist = -1;
    switch(the_obj->type) {
      case SPHERE:
        tmpdist = intersect_sphere(this_ray,the_obj,int_point);
        break;
      /* never any intersections for below */
      case NONE:
      default:
        break;
    }
    if((tmpdist >= 0.0) && (tmpdist < dist)) {
      dist = tmpdist;
      res  = the_obj;
    }
  }
  *rdist = dist;
  return res;
}

vect point_normal(const vect *point, const object *intobj) {
  vect res = {0};
  switch(intobj->type) {
    case SPHERE:
      /* just normalise a vector from origin to point */
      res = vect_sub(point,&intobj->origin);
      res = vect_norm(&res);
      break;
    default:
      break;
  }
  return res;
}

img_t shade_normal(const vect *cam, const vect *int_point,
                   const object *int_object, const object *the_light) {
  img_t res = 0;
  /* Using Phong lighing model: diffuse + specular highlights */
  vect l_to_i   = vect_sub(&the_light->origin,int_point);
  l_to_i        = vect_norm(&l_to_i);
  vect int_norm = point_normal(int_point,int_object);
  /* diffuse lighting */
  double inten  = int_object->diff * vect_dot(&l_to_i,&int_norm);
  /* Apply lighting */
  if (inten > 0.0) {
    inten = clamp(inten,1.0);
    res = rgba_scale(rgba_chan_scale(int_object->col,the_light->col),inten);
  }
  /* specular lighting */
  if (int_object->shiny > 0.0) {
    img_t specr   = the_light->col;
    vect view_dir = vect_sub(int_point,cam);
    view_dir      = vect_norm(&view_dir);
    vect r_dir    = vect_reflect(&l_to_i,&int_norm);
    double dvn    = vect_dot(&view_dir,&r_dir);
    if (dvn > 0.0) {
      inten       = pow(dvn,20) * int_object->shiny;
      specr       = rgba_scale(specr,inten);
      res         = rgba_add(res,specr);
    }
  }
  return res;
}

img_t shade(const ray *src_ray, const vect *int_point, const object *int_object,
            const object *lights, unsigned int num_lights, double ambient,
            unsigned reflections, const object *objects,
            unsigned int num_objects) {
  /* first ambient */
  img_t res = rgba_scale(int_object->col,ambient);
  unsigned int light;
  /* now light sources */
  for (light=0; light < num_lights; light++) {
    const object *the_light = &lights[light];
    /* check for path to the light -- first order shadows TODO -- needs objs */
    vect i_to_l = vect_sub(&the_light->origin,int_point);
    i_to_l      = vect_norm(&i_to_l);
    ray  to_l_r = {vect_sub(int_point,&i_to_l),i_to_l};
    double distl = 0.0, disto = DBL_MAX;
    const object *first_int = intersect(&to_l_r,lights,num_lights,&i_to_l,
                                        &distl);
    const object *first_obj = intersect(&to_l_r,objects,num_objects,&i_to_l,
                                         &disto);
    if (first_int == the_light && (!first_obj) ) {
      img_t newp;
      newp = shade_normal(&src_ray->origin,int_point,int_object,
                           &lights[light]);
      res = rgba_add(res,newp);
    }
  }
  /* now reflections */
  if (reflections > 0 && int_object->shiny > 0.0) {
    vect norm = point_normal(int_point,int_object);
    ray refl  = {vect_sub(int_point,&norm),vect_reflect(&src_ray->dir,&norm)};
    img_t reflp;
    const object *rayint = trace_ray(&reflp,&refl,lights,num_lights,ambient,
                                     reflections-1, objects, num_objects);
    if (rayint && (rayint != int_object)) {
      res = rgba_add(res,rgba_scale(reflp,int_object->shiny));
    }
  }
  return res;
}

const object* trace_ray(img_t *shadev, const ray *new_ray,const object *lights,
                        unsigned int num_lights, double ambient,
                        unsigned reflections, const object *objects,
                        unsigned int num_objects) {
  const object *res = 0;
  /* check for intersection  (result stored to pos)   */
  vect   pos = {0};
  double dist = 0.0;
  res = intersect(new_ray, objects, num_objects, &pos, &dist);
  /* do shading if intersetion  */
  if (res) {
    *shadev = shade(new_ray, &pos, res, lights, num_lights,ambient,
                    reflections, objects, num_objects);
  }
  return res;
}

int render_scene(scene *src_scene, image *targ_image, double image_scale) {
  /* iterate over output domain */
  unsigned int x, y;
  img_t *lin_pos = (unsigned int *)targ_image->data;
  for(y=0; y < targ_image->height; y++) {
    for (x=0; x < targ_image->width; x++) {
      /* calc this position */
      vect pos = plane_coord(&src_scene->view_plane,x,y,image_scale);
      /* calc ray for this position */
      ray newray = {0};
      newray.origin = src_scene->camera;
      newray.dir    = vect_sub(&pos,&src_scene->camera);
      newray.dir    = vect_norm(&newray.dir);
      /* do the actual ray-tracing */
      (void)trace_ray(lin_pos,&newray, src_scene->lights,
                      src_scene->num_lights, src_scene->ambient,
                      src_scene->reflections, src_scene->objects,
                      src_scene->num_objects);
      /* increment data position */
      lin_pos++;
    }
  }
  return 0;
}

int write_pbm(image *src_image, char *target) {
  FILE *img = fopen(target,"w");
  fprintf(img,"P6\n%u %u\n255\n",src_image->width,src_image->height);
  unsigned int length = src_image->width * src_image->height * sizeof(img_t);
  unsigned int pos;
  /* loops over RGBA data */
  for (pos = 0; pos < length; pos+=4) {
    /* write out RGB channels */
    fputc(src_image->data[pos]  ,img); /* R */
    fputc(src_image->data[pos+1],img); /* G */
    fputc(src_image->data[pos+2],img); /* B */
  }
  fclose(img);
  return 0;
}

/*-------------------------------------------------------------------------- */

#ifdef PROVIDE_MAIN
int main(int argc,char** argv) {
  /* declare mandatory objects */
  scene a_scene = {0};
  image t_image = {0,3840,2160};
  /* alloc image */
  alloc_image(&t_image);
  /* configure camera and view-plane */
  a_scene.camera          = (vect){0.0,0.0,-7000.0};
  a_scene.view_plane.tlc  = (vect){-960,-540,50.0};/* aspect ratio & FOV */
  a_scene.view_plane.xdir = (vect){1.0,0.0,0.0};   /* orientation */
  a_scene.view_plane.ydir = (vect){0.0,1.0,0.0};
  /* add some primitives to the scene */
  a_scene.objects[0] = (object)
    {SPHERE, rgb(200,20,20),   {-600.0,0.0,1200.0}, {500.0,0.0,0.0},0.8,0.5};
  a_scene.objects[1] = (object)
    {SPHERE, rgb(100,255,77),  {-400,0.0,500.0},    {300.0,0.0,0.0},0.8,0.5};
  a_scene.objects[2] = (object)
    {SPHERE, rgb(190,250,200), {800.0,0.0,500.0},   {200.0,0.0,0.0},0.8,0.5};
  a_scene.num_objects = 3;
  /* add some light sources */
  a_scene.lights[0]   = (object)
    {SPHERE,rgb(255,200,255), {20000.0,-20000.0,-20000.0}, {10.0,0.0,0.0}};
  a_scene.lights[1]   = (object)
    {SPHERE,rgb(120,255,255), {-20000.0,20000.0,-20000.0}, {10.0,0.0,0.0}};
  a_scene.num_lights  = 2;
  a_scene.ambient     = 0.001;
  a_scene.reflections = 10;
  /* render scene (last param is scale from img space to render space) */
  render_scene(&a_scene, &t_image, 0.5);
  /* store image to PBM */
  write_pbm(&t_image,"./render.pbm");
  /* de-alloc image */
  dealloc_image(&t_image);
}
#endif
